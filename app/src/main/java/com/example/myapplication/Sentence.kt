package com.example.myapplication

class Sentence {

    var call: CallSession = CallSession()
    var call_id: String = ""
    var user_id: String = ""
    var text: String = ""
    var recording: String = ""
    var start: String = ""
    var end: String = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
}