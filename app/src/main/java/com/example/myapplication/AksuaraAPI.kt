package com.example.myapplication

import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Part

interface AksuaraAPI {

    @GET("api/call/map")
    fun getMap () : Call<SentenceList>

    @POST("api/call/start")
    fun startCall () : Call<CallSession>

    @POST("api/call/stop")
    fun stopCall () : Call<CallSession>

    @Multipart
    @POST("api/call/speak")
    fun postSpeech(
        @Part("call_id") call_id: RequestBody,
        @Part("start") start: RequestBody,
        @Part("end") end: RequestBody,
        @Part("latitude") latitude: RequestBody,
        @Part("longitude") longitude: RequestBody,
        @Part audio: MultipartBody.Part
    ): Call<Sentence>
}