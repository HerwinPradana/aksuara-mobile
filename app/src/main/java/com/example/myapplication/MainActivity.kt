package com.example.myapplication

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.myapplication.ui.theme.MyApplicationTheme
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.Marker
import com.google.maps.android.compose.MarkerState
import com.google.maps.android.compose.rememberCameraPositionState
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : ComponentActivity(), LocationListener {

    private lateinit var locationManager: LocationManager
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private var currentLocation = mutableStateOf(Location(LocationManager.GPS_PROVIDER))
    private var currentZoom = 10f

    private var sentenceList = mutableStateOf(SentenceList())
    private lateinit var apiUrl: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (
            ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {
            val permissions = arrayOf(
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            ActivityCompat.requestPermissions(this, permissions, 0)
        }
        val metaData: Bundle = packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA).metaData
        apiUrl = metaData.getString("com.example.myapplication.API_URL").toString()

        getSentences()

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5f, this)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient.lastLocation.addOnSuccessListener {
            currentLocation.value = it

            val context = this
            setContent {
                MyApplicationTheme {
                    // A surface container using the 'background' color from the theme
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        Column (modifier = Modifier.padding(5.dp)) {
                            CallButton(context, onClick = { startCall() })
                            val center = LatLng(currentLocation.value.latitude, currentLocation.value.longitude)
                            val cameraPositionState = rememberCameraPositionState {
                                position = CameraPosition.fromLatLngZoom(center, currentZoom)
                            }
                            GoogleMap(
                                modifier = Modifier.fillMaxSize(),
                                cameraPositionState = cameraPositionState
                            ) {
                                if (sentenceList.value.items != null) {
                                    for (sentence in sentenceList.value.items!!) {
                                        val loc = LatLng(sentence.latitude, sentence.longitude)
                                        Marker(
                                            state = MarkerState(position = loc),
                                            title = sentence.text,
                                            snippet = "Call: " + sentence.call.code
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun startCall () {
        val retrofit = Retrofit.Builder().baseUrl(apiUrl).addConverterFactory(GsonConverterFactory.create()).build()
        val vttAPI = retrofit.create(AksuaraAPI::class.java)

        val call = vttAPI.startCall()
        val context = this
        call.enqueue(object: Callback<CallSession> {
            override fun onResponse(call: Call<CallSession>, response: Response<CallSession>) {
                if (response.isSuccessful) {
                    val callSession = response.body()
                    if (callSession !== null) {
                        val intent = Intent(context, CallActivity::class.java)
                        intent.putExtra("CALL_ID", callSession.id)
                        startActivity(intent)
                    }
                }
            }
            override fun onFailure(call: Call<CallSession>, t: Throwable) {
                Log.e("AksuaraAPI.startCall()", t.toString())
            }
        })
    }
    private fun getSentences () {
        val retrofit = Retrofit.Builder().baseUrl(apiUrl).addConverterFactory(GsonConverterFactory.create()).build()
        val vttAPI = retrofit.create(AksuaraAPI::class.java)

        val call = vttAPI.getMap()
        call.enqueue(object: Callback<SentenceList> {
            override fun onResponse(call: Call<SentenceList>, response: Response<SentenceList>) {
                val sentences = response.body()
                if (sentences !== null) {
                    sentenceList.value = sentences
                }
            }
            override fun onFailure(call: Call<SentenceList>, t: Throwable) {
                Log.e("API Error", t.toString())
            }
        })
    }

    override fun onLocationChanged(location: Location) {
        currentLocation.value = location
    }
}

@Composable
fun CallButton(context: Context, onClick: ()-> Unit) {
    val buttonModifier = Modifier.width(LocalConfiguration.current.screenWidthDp.dp)
    Button(
        onClick = onClick,
        modifier = buttonModifier
    ){
        Icon(Icons.Filled.Phone, context.getString(R.string.start_call))
        Text(context.getString(R.string.start_call))
    }
}

@Preview(showBackground = true)
@Composable
fun MainPreview() {
    MyApplicationTheme {
        val center = LatLng(1.35, 103.87)
        val cameraPositionState = rememberCameraPositionState {
            position = CameraPosition.fromLatLngZoom(center, 10f)
        }
        Column (modifier = Modifier.padding(5.dp)) {
            CallButton(LocalContext.current, onClick = {})
            GoogleMap(
                modifier = Modifier.fillMaxSize(),
                cameraPositionState = cameraPositionState
            ) {
                Marker(
                    state = MarkerState(position = center),
                    title = "Singapore",
                    snippet = "Marker in Singapore"
                )
            }
        }
    }
}