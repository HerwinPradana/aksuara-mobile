package com.example.myapplication

import android.Manifest
import android.content.ContentValues
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.myapplication.ui.theme.MyApplicationTheme
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit

class CallActivity : ComponentActivity(), LocationListener {

    private lateinit var apiUrl: String

    private var player: MediaPlayer? = null
    private var recorder: MediaRecorder? = null
    private lateinit var locationManager: LocationManager

    private val mediaFormat = MediaRecorder.OutputFormat.MPEG_4
    private val audioEncoding = MediaRecorder.AudioEncoder.DEFAULT

    private lateinit var callId: String
    private lateinit var currentRecordingFile: String
    private lateinit var currentStart: String
    private lateinit var currentEnd: String
    private lateinit var currentLocation: Location

    private val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")

    private var translations = mutableStateListOf<Sentence>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        callId = intent.getStringExtra("CALL_ID").toString()

        if (
            ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {
            val permissions = arrayOf(
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            ActivityCompat.requestPermissions(this, permissions, 0)
        }
        val metaData: Bundle = packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA).metaData
        apiUrl = metaData.getString("com.example.myapplication.API_URL").toString()

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5f, this)

        val context = this
        setContent {
            MyApplicationTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Column (modifier = Modifier.padding(5.dp)) {
                        PushButton(context, onPress = { startRecording() }, onRelease = { stopRecording() })
                        LazyColumn {
                            items(translations) {
                                SpeechDisplay(context, it.text, onStart = { startPlaying(it.recording) }, onStop = { stopPlaying() })
                            }
                        }
                    }
                }
            }
        }
    }

    private fun startRecording () {
        val num = translations.count()
        currentRecordingFile = "$externalCacheDir/recording_$num.3gp"
        currentStart = LocalDateTime.now().format(dateFormatter)
        currentEnd = ""

        recorder = MediaRecorder(this).apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(mediaFormat)
            setAudioEncoder(audioEncoding)
            setOutputFile(currentRecordingFile)

            try {
                prepare()
            } catch (e: IOException) {
                Log.e("startRecording", e.toString())
            }

            start()
        }
    }
    private fun stopRecording () {
        recorder?.apply {
            stop()
            release()
        }
        recorder = null

        postSpeech()
    }
    private fun postSpeech () {
        currentEnd = LocalDateTime.now().format(dateFormatter)

        val client = OkHttpClient.Builder().connectTimeout(5, TimeUnit.MINUTES).readTimeout(5, TimeUnit.MINUTES).writeTimeout(60, TimeUnit.SECONDS).build()
        val retrofit = Retrofit.Builder().baseUrl(apiUrl).client(client).addConverterFactory(GsonConverterFactory.create()).build()
        val vttAPI = retrofit.create(AksuaraAPI::class.java)

        val requestCall = RequestBody.create(MediaType.parse("text/plain"), callId)
        val requestStart = RequestBody.create(MediaType.parse("text/plain"), currentStart)
        val requestEnd = RequestBody.create(MediaType.parse("text/plain"), currentEnd)
        val requestLat = RequestBody.create(MediaType.parse("text/plain"), currentLocation.latitude.toString())
        val requestLng = RequestBody.create(MediaType.parse("text/plain"), currentLocation.longitude.toString())

        val recording = File(currentRecordingFile)
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), recording)
        val formData = MultipartBody.Part.createFormData("audio", recording.name, requestFile)

        val call = vttAPI.postSpeech(requestCall, requestStart, requestEnd, requestLat, requestLng, formData)
        call.enqueue(object: Callback<Sentence> {
            override fun onResponse(call: Call<Sentence>, response: Response<Sentence>) {
                if (response.isSuccessful) {
                    val sentence = response.body()
                    if (sentence !== null) {
                        translations.add(sentence)
                        writeCsv()
                    }
                }
            }
            override fun onFailure(call: Call<Sentence>, t: Throwable) {
                Log.e("AksuaraAPI.posSpeech()", t.toString())
            }
        })
    }

    private fun startPlaying (audio: String?) {
        if (audio != null) {
            player = MediaPlayer().apply {
                try {
                    setDataSource(audio)
                    prepare()
                    start()
                } catch (e: IOException) {
                    Log.e("startPlaying", e.toString())
                }
            }
        }
    }
    private fun stopPlaying () {
        player?.release()
        player = null
    }

    override fun onStop() {
        super.onStop()

        val retrofit = Retrofit.Builder().baseUrl(apiUrl).addConverterFactory(GsonConverterFactory.create()).build()
        val vttAPI = retrofit.create(AksuaraAPI::class.java)

        val call = vttAPI.stopCall()
        call.enqueue(object: Callback<CallSession> {
            override fun onResponse(call: Call<CallSession>, response: Response<CallSession>) {
            }
            override fun onFailure(call: Call<CallSession>, t: Throwable) {
                Log.e("AksuaraAPI.stopCall()", t.toString())
            }
        })
    }

    override fun onLocationChanged(location: Location) {
        currentLocation = location
    }

    private fun writeCsv () {
        var content = "call_id,text,recording,start,end,latitude,longitude\n"
        translations.forEach{
            content += it.call_id + "," + it.text + "," + it.recording + "," + it.start + "," + it.end + "," + it.latitude + "," + it.longitude + "\n"
        }

        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, callId)
            put(MediaStore.MediaColumns.MIME_TYPE, "text/csv")
            put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS)
        }

        val uri = contentResolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues)
        if (uri != null) {
            val dst = contentResolver.openOutputStream(uri)
            dst?.write(content.toByteArray())
            dst?.close()
        }
    }
}

@Composable
fun SpeechDisplay(context: Context, text: String?, onStart: ()-> Unit, onStop: ()->Unit) {
    var isPlaying by remember { mutableStateOf(false) }
    var icon by remember { mutableStateOf(Icons.Filled.PlayArrow) }
    var iconDesc by remember { mutableStateOf(context.getString(R.string.start_playing)) }

    val buttonWidth = 75.dp
    Row {
        Column {
            Text(text = text ?: "", modifier = Modifier.width(LocalConfiguration.current.screenWidthDp.dp - buttonWidth))
        }
        Column {
            Button(
                onClick = {
                    isPlaying = !isPlaying
                    if (isPlaying) {
                        icon = Icons.Filled.Close
                        iconDesc = context.getString(R.string.stop_playing)
                    } else {
                        icon = Icons.Filled.PlayArrow
                        iconDesc = context.getString(R.string.start_playing)
                    }
                    if (isPlaying) {
                        onStart()
                    } else {
                        onStop ()
                    }
                },
                modifier = Modifier.width(buttonWidth)
            ){
                Icon(icon, iconDesc)
            }
        }
    }
}

@Composable
fun PushButton(context: Context, onPress: ()-> Unit, onRelease: ()->Unit) {
    val interactionSource = remember { MutableInteractionSource() }
    val isPressed by interactionSource.collectIsPressedAsState()

    var text by remember { mutableStateOf(context.getString(R.string.push_to_talk)) }

    if (isPressed) {
        text = context.getString(R.string.release_to_stop)
        onPress()

        DisposableEffect(Unit) {
            onDispose {
                text = context.getString(R.string.push_to_talk)
                onRelease()
            }
        }
    }

    val buttonModifier = Modifier.width(LocalConfiguration.current.screenWidthDp.dp)
    Button(
        onClick = {},
        interactionSource = interactionSource,
        modifier = buttonModifier
    ){
        Icon(Icons.Filled.Phone, text)
        Text(text)
    }
}

@Preview(showBackground = true)
@Composable
fun CallPreview() {
    MyApplicationTheme {
        Column (modifier = Modifier.padding(5.dp)) {
            PushButton(LocalContext.current, onPress = {}, onRelease = {})
            LazyColumn {
                items (5) { index ->
                    SpeechDisplay(LocalContext.current, text = "Speech translation $index", onStart = {}, onStop = {})
                }
            }
        }
    }
}